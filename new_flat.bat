@echo off
rem See :HELP for explanation

if "%1"=="" goto HELP

set projname=flat_%1
echo Creating new project: %projname%
md %projname%
copy empty_flat %projname%
cd %projname%
ren empty_flat.* %projname%.*
cd ..

goto END

:HELP
echo This script will create a new flat module project from template.
echo Usage: new_flat ^<pcb_name^>
echo New project directory will be named flat_^<pcb_name^>
echo Basic project files as well.

:END