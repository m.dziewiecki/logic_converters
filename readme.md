The BABEL project
=================
(B-word) Adapters for Beam Electronics
--------------------------------------

Introduction
------------------

This is an attempt to standarize logic conversion at GSI.
Over years, many different logic standards have been used for connecting triggers, interlocks, alarms and other
logic signals between various devices.  
The problem of interconnecting them all together arises as in the times when platforms are replaced by new ones 
(partly or completely), mostly with different signalling.

The concept is that we create a system with standarized input and output mosules (_boards_) with common backend interface, 
so that they can be easily conncted together to create _anything-to-anything_ interfaces.  
Further, the standard aims to be simple enough to allow rapid prototyping and even making 'quick and dirty'
solutions using breadboards and similar technologies.

Basics
------

Each interface consists of at least one input board, at least one output board and at least one power supply.  
All of them are connected using a common interconnect or _bus_ (which is __not__ a bus in a traditional sense), either directly
or by means of a backplane (see below).  

In any case, a common rule exists: signals are __commonly__ fed from left to right.   
That means: in a setup consisting of several boards, any output is to the right of its corresponding input.  
Or: a board takes signals from its left and ransmits signal to the right.
This rule may be violated if custom backplanes, links or patch units of any kind are used. 


Interconnect
------------

### Signals ###

Each board provides up to 16 inputs and 16 outputs.  
Following general rules apply: 
- a board may take signals from its left neighbour to output them to the world;  
we call such signals __B2W__ (Bus to World)
- a board may put signals from the world to its right neighbour;  
we call such signals __W2B__ (World to Bus)
- if less than 16 signals are used, these are always ones with lowest numbers  
(we number signals from 0 to 15)
- unused signals are directly forwarded from the left to the right

Especially the last rule is ver important for creating bigger systems with multiple boards.  
Thanks to signal forwarding, e.g. a 16-fold W2B board can distribute its signals to following four quadruple B2W boards,
or eight double boards, or one 8-fold and eight single output boards, or any other combination.

### Logic standard ###

The common interface between boards is TTL or 5V or 3.3V with 5V-tolerant inputs.
Following rules apply here:
- each W2B output must be TTL-level compatible (<0.5 V for low, >2.7 V for high)
- each W2B output must source at least 10 mA at TTL high and sink at least 10mA at TTL low
- each B2W input must be TTL-level compatible (<0.8 V for 0, >2.0 V for 1)
- each B2W input must not draw nor source more than 1 mA at any voltage between 0 and 5.2 V
- each B2W input must be 5V-tolerant even if there's 3.3 V logic used behind it
- unconnected B2W inputs should default to low.

### Power ###

Besides signal lines, there are two-fold-redundant power lines with +24 V and +5.5 V.  
The two lines are called 'primary' and 'secondary'. Boards may use only the primary line or both of them,
but never only the secondary one.  
It's forbidden to directly short primary and secondary lines. Typically,
standard sillicon or Schottky diodes are used to isolate primary and secondary lines from each other.

The 'additional' 0.5 V over common 5 V is to allow easy power redundancy implementation with two sillicon diodes.

### Status lines ###

There are also two status lines for signaling errors. These are open-collector type.
All outputs are wire-anded and low state on the line means an error.  
The pull-up resistor is normally located at the signal receiver. It should be tied to +5V and its resistance should be 
between 2.2 kOhm and 4.7 kOhm.    
All status outputs should be able to sink at least 10 mA at 0.5 V. Further, all status inputs and outputs should withstand +24 V.

Use of status lines is not obligatory.

Mechanical standard
-------------------

There are two standards developed by now:
- "__flat type__" where various boards are put next to each other and connected via edge connectors
- "__plugin type__" where various boards are put on a backplane (which is customizable)

Both of them can be applied in various mechanical environments like DIN rail, 19" enclosure or custom box.

### The "flat type": ###
- (+) is cheaper and mechanically less complicated
- (+) is flat, which is best option for DIN rail-mounted devices
- (+) allows adjusting PCB length to area requirements
- (-) custom signal routing (other than left-to-right) is hard 
- (-) occupied area is quite high
- (-) equipping it with front plate may be problematic

It is well suited for simple (low channel count), DIN-rail-based designs.

### The "plugin type": ###
- (+) has more compact mechanical design
- (+) allows custom routing (other than left-to-right)
- (-) needs a backplane, which is an additional cost and effort
- (-) has fixed PCB size

It is better suited for 19"-based designs with many I/Os.

Creating new projects
---------------------

To create a new project, use one of batch scripts. It will copy a template directory (`empty_*`) to a new directory and rename
files accordingly.  
This way user gets a configured project with all needed components and a template of PCB, so that many annoying
activities (configuring libraries, defining board size, placement of standard connectors, front panel etc.) don't need to be 
repeated for each project.

Managing projects
-----------------

With lots of interfaces, it's easy to get lost among lots of various boards.  
However, there's no management system and board types become no numbers - only descriptive names.  
I (Michal) believe, that managing all these simple projects introduces more effort than developing them.
So, if you need, make yourself a catalogue. BABEL means chaos.

Standard components
-------------------

To ease rapid prototyping and make use of stored components, always consider using the following components in your designs:

### Connectors: ###
- for LEMO/NIM: EPA.00.250.NTN
- for 24V in/out: Phoenix MC 3.5mm series
- for standard optical links: HFBR1414/HFBR2412; there is a mezzanine PCB (HFBR_mount) for straight mounting

### Resistors: ###
Any, but if possible, 0805, thick film, 1%

### Capacitors: ###
Any, but if possible, 0805

### Diodes: ###
- high-current rectifiers: US1J (sillicon) or SK110 (Schottky)
- low-current rectifiers: BAS316
- double small-signal: BAV99/BAV199
- ESD for 24V lines (bi-dir): PESD24VL1BA
- ESD for 24V lines (uni-dir): PESD24S1UA
- ESD for 24V lines (uni-dir, high power): SMBJ24A
- ESD for 5V lines (bi-dir): PESD5V0L1BA

### Transistors: ###
- small-signal NPN: BC817
- small-signal PNP: BC807
- 1W NPN: BCP56
- 1W PNP: BCP53

### ICs ###
- Active optocoupler: 6N137S
- 1-ch inverter: 74LVC1G04, SC-70 (TSOP-5)
- 8-ch buffer: 74HCT541 (SOW-20)
- 8-ch inverting buffer: 74HCT540 (SOW-20)
- single AND: 74LVC1G08 (TSOP-5)

Bill of materials
-----------------

There's a common BOM file (common_bom.xlsx) which summarizes all components for all boards to ease batch ordering.
Please integrate your BOM into this file once you are ready with your project. Try not to produce duplicates
and sort data by component name afterwards.