@echo off
rem See :HELP for explanation

if "%1"=="" goto HELP

set projname=plugin_%1
echo Creating new project: %projname%
md %projname%
copy empty_plugin %projname%
cd %projname%
ren empty_plugin.* %projname%.*
cd ..

goto END

:HELP
echo This script will create a new plugin module project from template.
echo Usage: new_plugin ^<plugin_name^>
echo New project directory will be named plugin_^<plugin_name^>
echo Basic project files as well.

:END