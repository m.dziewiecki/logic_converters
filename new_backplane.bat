@echo off
rem See :HELP for explanation

if "%1"=="" goto HELP

set projname=backplane_%1
echo Creating new project: %projname%
md %projname%
copy empty_backplane %projname%
cd %projname%
ren empty_backplane.* %projname%.*
cd ..

goto END

:HELP
echo This script will create a new backplane project from template.
echo Usage: new_backplane ^<backplane_name^>
echo New project directory will be named backplane_^<backplane_name^>
echo Basic project files as well.

:END